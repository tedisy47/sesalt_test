
# Documentasi


## Instalasi
	
	Untuk menjalankan aplikasi ini silahkan mengitu langkah-langkah berikut:

	- clone https://gitlab.com/tedisy47/sesalt_test.git
	- Buat file .env atau bisa rename vile .env.exaple lalu setiing database yang berada di file .env
	- Menginstall dengan composer di terminal atau commandline dengan perintah composer install
	- Melakukan migrasi database dengan melakukan perintah php artisan migrate
	- Membuat data denggan seeder dengan menggunakan perintah php artisan db:seed atau jika inggin membuat data hanya dengan class tertentu anda bisa nggunakan perintah sebagai berikut
			- php artisan db:see -- UserSeeder (untuk membuat data user)
			- php artisan db:see -- Notificationseeder (untuk membuat data Notifications)
			- php artisan db:see -- UserSeeder (untuk membuat data user)
			- php artisan db:seed -- Notification_readsSeeder (untuk membuat data notification_read)
	- Menjalankan server local dengan perintah php artisan serve di Terminal atau comandline

## Cara menggunakan
	
	untuk mengakses API laravel bisa menggunakan postman atau browser anda dengan mengakses link sebagai berikut

### user

#### get all user
* link: http://localhost:8000/api/user
* mehtode: get
* result: menampilkan semua data yang ada dalam table user

#### get  Userby ID
* link: http://localhost:8000/api/user/{id}
* mehtode: get
* result: menampilkan data berdasarkan user.id


#### get all user
* link: http://localhost:8000/api/user
* mehtode: get
* result: menampilkan semua data yang ada dalam table user

### Notification


#### get all notification
* link: http://localhost:8000/api/notification
* mehtode: get
* result: menampilkan semua data yang ada dalam table Notification

#### get Notification by ID
* link: http://localhost:8000/api/notification/{id}
* mehtode: Get
* result: menampilkan semua data Berdasarkan notification.id


#### get Notification by user_id
* link: http://localhost:8000/api/notification/by_user/{id}
* mehtode: Get
* result: menampilkan data Notification berdasarkan User_ID


### catatan

* untul menjawab soal A point 3 silahkan mengakses http://localhost:8000/api/notification/by_user/1 karena user.id dengan diki

* untuk menjawab soal B point 1 silahkan untuk mengaksesn http://localhost:8000/qwerty
* untuk menjawab soal B point 2 silakan untuk mengakses http://localhost:8000/queue

