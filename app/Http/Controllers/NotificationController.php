<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Notification\NotificationInterface as NotificationInterface;


class NotificationController extends Controller
{
    private $NotificationRepository;

    public function __construct(NotificationInterface $NotificationRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
    }
    public function index()
    {
         $Notification = $this->NotificationRepository->getAllPagination(5);
         return response()->json($Notification, 200);
    }
    public function find($id)
    {
        return $Notification = $this->NotificationRepository->findById($id);
    }
    public function findByUser($user_id)
    {
        return $Notification = $this->NotificationRepository->getById_user($user_id);
    }
}