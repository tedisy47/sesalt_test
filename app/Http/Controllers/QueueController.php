<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Jobs\QueueJob;

class QueueController extends Controller
{
    public function index()
    {
    	$details['do'] = 'test';
        dispatch(new QueueJob($details));
        return $details;
    }
    
}