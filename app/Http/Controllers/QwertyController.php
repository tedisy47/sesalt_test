<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QwertyController extends Controller
{
    public function index()
    {
    	$string = 'QWERTYUIOP';
    	$array = str_split($string, 1);
    	$array = array_reverse($array);
    	$result =  implode($array);
    	return $result;
    }
    
}