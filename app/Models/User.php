<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class User extends Model
{
   
    protected $fillable = [
        'name',
        'age',
    ];
     public function notifications()
    {
        return $this->hasMany(Notifications::class);
    }

    
}
