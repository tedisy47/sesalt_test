<?php

namespace App\Repositories\Notification;

/*

intetface notification


*/

interface NotificationInterface {
    public function findById(int $id);
    public function getAllPagination($page);
    public function getById_user(int $user_id);
}