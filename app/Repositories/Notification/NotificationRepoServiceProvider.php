<?php
namespace App\Repositories\Notification;
use Illuminate\Support\ServiceProvider;

class NotificationRepoServiceProvider extends ServiceProvider{
    public function boot(){}

    public function register(){
        $this->app->bind('App\Repositories\Notification\NotificationInterface', 
        'App\Repositories\Notification\NotificationRepository');
    }
}