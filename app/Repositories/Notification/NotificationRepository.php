<?php
namespace App\Repositories\Notification;

use App\Repositories\Notification\NotificationInterface as NotificationInterface;

use App\Models\Notifications;
class NotificationRepository implements NotificationInterface{

    protected $notification;

	public function __construct(Notifications $notification)
	{
        $this->notification = $notification;
    }


    /*
    * get data by ID
    * @param int $id
    * @return User
    */

    public function findById(int $id)
    {
        return $this->notification->find($id);
    }
    public function getAllPagination($page)
    {
        return $this->notification->paginate($page);
    }
    public function getById_user(int $user_id)
    {
        return $this->notification
        ->select('name','category','title','description','notifications.created_at','is_read','notifications_reads.created_at as read_at')
        ->join('notifications_reads', 'notifications_reads.notification_id','=','notifications.id','left')
        ->join('users', 'users.id','=','notifications.user_id','left')
        ->where('notifications_reads.user_id','=',$user_id)
        ->orWhere('notifications.user_id','=', $user_id)
        ->get();
    }
}