<?php

namespace App\Repositories\User;

interface UserInterface {
    public function findById(int $id): User;
    public function getAllPagination($page);
}