<?php
namespace App\Repositories\User;

use App\Repositories\User\UserInterface as UserInterface;

use App\Models\User;
class UserRepository implements UserInterface{

    protected $user;

	public function __construct(User $user)
	{
        $this->user = $user;
    }

    /*
    * get data by ID
    * @param int $id
    * @return User
    */
    public function findById(int $id): User
    {
        return $this->user->find($id);
    }

    public function getAllPagination($page): User
    {
        return $this->user->paginate($page);
    }
}