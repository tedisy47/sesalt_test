<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Notifications_read;

class Notification_readsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $user = [
            [
               'is_read'=>1,
               'notification_id'=>1,
               'user_id'=>1
            ],
            [
               'is_read'=>1,
               'notification_id'=>2,
               'user_id'=>1
            ],
            [
               'is_read'=>1,
               'notification_id'=>3,
               'user_id'=>3
            ],
            [
               'is_read'=>1,
               'notification_id'=>3,
               'user_id'=>2
            ],
        ];
  
        foreach ($user as $key => $value) {
            Notifications_read::create($value);
        }
    }
}
