<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Notifications;


class Notificationseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $user = [
            [
               'title'=>'pembayaran',
               'description'=>'pembayaran anda diterima',
               'category'=>'PAYMENT',
               'user_id' => 1
            ],
            [
               'title'=>'Promo',
               'description'=>'Deskripsi Promo',
               'category'=>'PROMO',
               'user_id' => null
            ],
            [
               'title'=>'pembayaran',
               'description'=>'pembayaran dibatalkan',
               'category'=>'PAYMENT',
               'user_id' => 1
            ],
            [
               'title'=>'pembayaran',
               'description'=>'segera melakukan pembayaran',
               'category'=>'PAYMENT',
               'user_id' => 3
            ],
        ];
  
        foreach ($user as $key => $value) {
            Notifications::create($value);
        }
    }
}
