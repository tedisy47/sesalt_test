<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
         $user = [
            [
               'name'=>'diki',
               'age'=>24,
            ],
            [
               'name'=>'indra',
               'age'=>28,
            ],
            [
               'name'=>'feby',
               'age'=>21,
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
