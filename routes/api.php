<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('user/', [App\Http\Controllers\UserController::class, 'index']);
Route::get('user/{id}', [App\Http\Controllers\UserController::class, 'find']);


Route::get('notification/', [App\Http\Controllers\NotificationController::class, 'index']);
Route::get('notification/{id}', [App\Http\Controllers\NotificationController::class, 'find']);
Route::get('notification/by_user/{id}', [App\Http\Controllers\NotificationController::class, 'findByUser']);
